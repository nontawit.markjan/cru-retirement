const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
    Username: String,
    Password: String,
})

module.exports = mongoose.model("Users", userSchema);