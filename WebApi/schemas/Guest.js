const mongoose = require('mongoose');
const { Schema } = mongoose;

const guestSchema = new Schema({
    GuestFullname: String,
    CongratMessage: String,
    Gender: String,
    SendingTime: Date,
    ProfilePircture: {
        ProfilePirctureId: Number,
        ProfilePircturePath: String,
        CreatedDate: Date
    },
})

module.exports = mongoose.model("Guests", guestSchema);