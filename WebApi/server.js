const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const mongoose = require('mongoose');
const dbConfig = require('./config/database.config');

const baseRouter = require('./routes/base.route');
const congratulatoryRouter = require('./routes/congratulatory.route');

const { Schema } = mongoose;
const userSchema = require('./schemas/User.js');
const guestSchema = require('./schemas/Guest.js');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/api', [baseRouter, congratulatoryRouter]);

// database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error:'));
db.once('open', () => { console.log("Database Connected!")})

// port
const port = process.env.PORT || '8000';
app.set('port', port);

const server = http.createServer(app);

// const user = new userSchema({ Username: 'username', Password: 'password'})
// user.save(function (err, user) {
//     if (err) return console.error(err);
//     else return console.log("Successfully saved user data!")
//   });

server.listen(port, function () {
    console.info(`Server is up and running on port ${port}`)
});