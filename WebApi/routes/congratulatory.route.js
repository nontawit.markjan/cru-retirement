const express = require('express');
const congratulatoryRouter = express.Router();

const guestSchema = require('../schemas/Guest.js');

congratulatoryRouter.post('/congratulatory/savedata', function (req, res) {
    const guest = new guestSchema({
        GuestFullname: req.body.GuestFullname,
        CongratMessage: req.body.CongratMessage,
        Gender: req.body.Gender,
        SendingTime: Date.now(),
        ProfilePicture: {
            ProfilePictureId: 1,
            ProfilePicturePath: 'document/guestProfilePicture/',
            CreatedDate: Date.now()
        }
    })

    guest.save(function (err, user) {
        if (err) return console.error(err);
        else return console.log("Successfully saved user data!")
    });

    res.send(req.body);
});

module.exports = congratulatoryRouter;