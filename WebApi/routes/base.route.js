const express = require('express');
const baseRouter = express.Router();

baseRouter.get('/', function (req, res) {
    res.send('API works!');
});

module.exports = baseRouter;