import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendanceStatComponent } from './attendance-stat.component';

describe('AttendanceStatComponent', () => {
  let component: AttendanceStatComponent;
  let fixture: ComponentFixture<AttendanceStatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttendanceStatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanceStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
