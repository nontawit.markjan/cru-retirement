import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AttendanceStatComponent } from './attendance-stat.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: AttendanceStatComponent
  },
  {
    path: '**',
    redirectTo: '',
  }
]

@NgModule({
  declarations: [
    AttendanceStatComponent
  ],
  imports: [
    RouterModule.forChild(routes),
  ]
})
export class AttendanceStatModule { }
