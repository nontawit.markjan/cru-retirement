import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RetirementEventComponent } from './retirement-event.component';

describe('RetirementEventComponent', () => {
  let component: RetirementEventComponent;
  let fixture: ComponentFixture<RetirementEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RetirementEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetirementEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
