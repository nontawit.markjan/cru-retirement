import { RetirementEventComponent } from './retirement-event.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: RetirementEventComponent
  },
  {
    path: '**',
    redirectTo: '',
  }
]

@NgModule({
  declarations: [RetirementEventComponent],
  imports: [
    RouterModule.forChild(routes),
  ]
})
export class RetirementEventModule { }
