import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RetirementEventComponent } from './retirement-event/retirement-event.component';
import { AttendanceStatComponent } from './attendance-stat/attendance-stat.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
  },
  {
    path: 'retirement-event',
    loadChildren: () => import('./retirement-event/retirement-event.module').then(m => m.RetirementEventModule)
  },
  {
    path: 'attendance-stat',
    loadChildren: () => import('./attendance-stat/attendance-stat.module').then(m => m.AttendanceStatModule)
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)]
})
export class TeacherModule { }
