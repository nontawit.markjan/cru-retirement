import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'congratulatory',
    loadChildren: () => import('./congratulatory/congratulatory.module').then(m => m.CongratulatoryModule),
  },
  {
    path: 'love-submission',
    loadChildren: () => import('./love-submission/love-submission.module').then(m => m.LoveSubmissionModule)
  },
  {
    path: '**',
    redirectTo: 'congratulatory',
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)]
})
export class StudentModule { }
