import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoveSubmissionComponent } from './love-submission.component';

describe('LoveSubmissionComponent', () => {
  let component: LoveSubmissionComponent;
  let fixture: ComponentFixture<LoveSubmissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoveSubmissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoveSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
