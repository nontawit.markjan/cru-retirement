import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoveSubmissionComponent } from './love-submission.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: LoveSubmissionComponent
  },
  {
    path: '**',
    redirectTo: '',
  }
]

@NgModule({
  declarations: [
    LoveSubmissionComponent
  ],
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class LoveSubmissionModule { }
