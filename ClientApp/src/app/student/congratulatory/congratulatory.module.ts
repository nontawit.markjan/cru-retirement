import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CongratulatoryComponent } from './congratulatory.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: CongratulatoryComponent
  },
  {
    path: '**',
    redirectTo: '',
  }
]

@NgModule({
  declarations: [
    CongratulatoryComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ]
})
export class CongratulatoryModule { }
