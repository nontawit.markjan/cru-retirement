import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-congratulatory',
  templateUrl: './congratulatory.component.html',
  styleUrls: ['./congratulatory.component.css']
})
export class CongratulatoryComponent implements OnInit {

  congratForm = new FormGroup({
    fullName: new FormControl(''),
    congratMessage: new FormControl(''),
    gender: new FormGroup({
      male: new FormControl(''),
      female: new FormControl(''),
    })
  });

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.log(this.congratForm.value);
  }
}
